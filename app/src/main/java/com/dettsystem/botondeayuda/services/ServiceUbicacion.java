package com.dettsystem.botondeayuda.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dettsystem.botondeayuda.api.VolleySingleton;
import com.dettsystem.botondeayuda.api.api;
import com.google.android.gms.location.LocationResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceUbicacion extends BroadcastReceiver {

    public static final String ACTION_PROCESS_UPDATE = "com.dettsystem.botondeayuda.services";

    @Override
    public void onReceive(final Context context, Intent intent) {

        if(intent != null ){
            final String action = intent.getAction();
            if(ACTION_PROCESS_UPDATE.equals(action)){
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null){
                    Location location = result.getLastLocation();
                    String location_string = new StringBuilder(""+location.getLatitude())
                            .append("/")
                            .append(location.getLongitude())
                            .toString();
                    final String lat = new StringBuilder(""+location.getLatitude()).toString();
                    final String lng = new StringBuilder(""+location.getLongitude()).toString();
                    //final String usuario = new StringBuilder(""+currentUser).toString();
                    final String direccion = "aqui";
                    //Toast.makeText(context,""+usuario,Toast.LENGTH_SHORT).show();
                    try{
                        // Inicial.getInstance().updateTextView(location_string);
                    }catch (Exception ex){
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, api.URL_CAPTURA_UBICACION,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try{
                                            JSONObject obj = new JSONObject(response);
                                            if (!obj.getBoolean("error")) {
                                                //Toast.makeText(context,"ubicacion guardada",Toast.LENGTH_SHORT).show();
                                            }else{
                                                //Toast.makeText(context,"Ocurrio un error al guardar la ubicacion",Toast.LENGTH_SHORT).show();
                                            }

                                        }catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context,"Ocurrio un error",Toast.LENGTH_SHORT).show();
                                    }
                                }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("direccion", direccion);
                                params.put("lat", lat);
                                params.put("lng", lng);
                                return params;
                            }
                        };
                        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
                        //Toast.makeText(context,location_string,Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }
}