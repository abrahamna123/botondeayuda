package com.dettsystem.botondeayuda.pushnotification;

import android.util.Log;

import androidx.annotation.NonNull;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.api.account.register.RegisterUserClientService;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.applozic.mobicomkit.api.notification.MobiComPushReceiver;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FcmListenerService extends FirebaseMessagingService {

    final private static String TAG = "FcmInstanceIDListener";
    private static final String TAG2 = "BontonPanicoGcmListener";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.i(TAG2,"Message data:" + remoteMessage.getData());

        if (remoteMessage.getData().size() > 0){
            if (MobiComPushReceiver.isMobiComPushNotification(remoteMessage.getData())){
                Log.i(TAG2,"Boton de panico notification processing...");
                MobiComPushReceiver.processMessageAsync(this,remoteMessage.getData());
                return;
            }
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        String registrationId = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG,"Found registration Id:" + registrationId);
        Applozic.getInstance(this).setDeviceRegistrationId(registrationId);
        if (MobiComUserPreference.getInstance(this).isRegistered()){
            try {
                RegistrationResponse registrationResponse = new RegisterUserClientService(this).updatePushNotificationId(registrationId);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
