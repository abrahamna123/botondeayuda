package com.dettsystem.botondeayuda.api;

public class api {

    //produccion
    private static final String URL_BASE_PROD = "https://saore.network/api-botonpanic/";

    // Concatenamos la URL con el archivo para llamada del API
    private static final String ROOT_URL = URL_BASE_PROD + "API.php?apicall=";

    //CAPTURA UBICACION
    public static final String URL_CAPTURA_USUARIO = ROOT_URL + "insertarusuario";

    //CAPTURA UBICACION
    public static final String URL_CAPTURA_UBICACION = ROOT_URL + "insertarubicacion";
}
