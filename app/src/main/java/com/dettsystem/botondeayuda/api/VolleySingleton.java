package com.dettsystem.botondeayuda.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {

    private static VolleySingleton mInstance;
    private RequestQueue mRequestqueue;
    private static Context mCtx;

    private VolleySingleton(Context context){
        mCtx = context;
        mRequestqueue = getRequestQueue();
    }

    public static synchronized VolleySingleton getInstance(Context context){
        if(mInstance == null){
            mInstance = new VolleySingleton(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if(mRequestqueue == null){
            mRequestqueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestqueue;
    }

    public <T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }
}
