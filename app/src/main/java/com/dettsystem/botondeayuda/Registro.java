package com.dettsystem.botondeayuda;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.listners.AlLoginHandler;
import com.applozic.mobicomkit.listners.AlPushNotificationHandler;
import com.applozic.mobicommons.commons.core.utils.Utils;

public class Registro extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private EditText mEmailView;
    private EditText mUserIdView;
    private EditText mPhoneNumberView;
    private EditText mPasswordView;
    private EditText mDisplayName;
    private Button mEmailSignInButton;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        Applozic.init(this,"16848f97c9587062b5308608120add6f4");

        mEmailView = (EditText) findViewById(R.id.email);
        mUserIdView = (EditText) findViewById(R.id.userId);
        mPasswordView = (EditText) findViewById(R.id.password);
        mDisplayName = (EditText) findViewById(R.id.displayName);

        mEmailSignInButton = (Button) findViewById(R.id.btnRegister);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.toggleSoftKeyBoard(Registro.this,true);
                registroApplozic(User.AuthenticationType.APPLOZIC);
            }
        });
    }

    private void registroApplozic(User.AuthenticationType authenticationType){
        String email  =  mEmailView.getText().toString();
        String userId = mUserIdView.getText().toString();
        String password = mPasswordView.getText().toString();
        String nombre = mDisplayName.getText().toString();


        User user =  new User();
        user.setUserId(userId);
        user.setEmail(email);
        user.setPassword(password);
        user.setDisplayName(nombre);
        user.setImageLink("");
        user.setAuthenticationTypeId(User.AuthenticationType.APPLOZIC.getValue());

        Applozic.loginUser(this, user, new AlLoginHandler() {
            @Override
            public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                Applozic.registerForPushNotification(Registro.this, new AlPushNotificationHandler() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {

                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {

                    }

                });
                //starting main MainActivity
                Intent mainIntent = new Intent(context,MainActivity.class);
                context.startActivity(mainIntent);
                Registro.this.finish();
            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                AlertDialog alertDialog = new AlertDialog.Builder(Registro.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage(exception.toString());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Alert",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if (!isFinishing()) {
                    alertDialog.show();
                }
            }
        });
    }
}
