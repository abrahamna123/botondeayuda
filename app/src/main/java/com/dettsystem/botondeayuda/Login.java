package com.dettsystem.botondeayuda;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.listners.AlLoginHandler;
import com.applozic.mobicomkit.listners.AlPushNotificationHandler;
import com.applozic.mobicommons.commons.core.utils.Utils;

public class Login extends AppCompatActivity {

    private EditText user;
    private EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Applozic.init(this,"16848f97c9587062b5308608120add6f4");

        user = findViewById(R.id.textUser);
        pass = findViewById(R.id.textPass);

        Button loginButton = findViewById(R.id.buttonLogin);
        Button registrButton = findViewById(R.id.buttonRegistro);

        if(user.getText().length() < 0){
            Log.i("UserId","Is empty");
            return;
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.toggleSoftKeyBoard(Login.this,true);
                login(User.AuthenticationType.APPLOZIC);
            }
        });

        registrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(Login.this,Registro.class);
                Login.this.startActivity(mainIntent);
                Login.this.finish();
            }
        });

    }

    private void login(User.AuthenticationType authenticationType){
        String usuario = user.getText().toString();
        String password = pass.getText().toString();


        User user =  new User();
        user.setUserId(usuario);
        user.setPassword(password);
        user.setImageLink("");
        user.setAuthenticationTypeId(authenticationType.getValue());

        Applozic.loginUser(this, user, new AlLoginHandler() {
            @Override
            public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                Applozic.registerForPushNotification(Login.this, new AlPushNotificationHandler() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {

                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {

                    }

                });
                //starting main MainActivity
                Intent mainIntent = new Intent(context,MainActivity.class);
                context.startActivity(mainIntent);
                Login.this.finish();
            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage(exception.toString());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Alert",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if (!isFinishing()) {
                    alertDialog.show();
                }
            }
        });
    }
}
